package db;

import constant.Query;
import model.Album;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class AlbumDao implements Dao<Album> {
    private Connection connection;
    private ArtistDao artistDao = new ArtistDao();

    @Override
    public Album get(int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.GET_ALBUM_QUERY);
        statement.setLong(1, id);
        ResultSet rs = statement.executeQuery();
        Album album = new Album();
        while (rs.next()) {
            album.setName(rs.getString("name"));
            album.setArtist(artistDao.get(rs.getInt("artist")));
        }
        return album;
    }

    @Override
    public Map<Integer, String> getAll() throws SQLException {
        connection = ConnectionManager.getConnection();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(Query.GET_ALL_ALBUM_QUERY);
        Map<Integer, String> map = new HashMap<>();
        while (rs.next()) {
            map.put(rs.getInt("id"), rs.getString("name"));
        }
        return map;
    }

    @Override
    public void create(Album album) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.ADD_ALBUM_QUERY);
        statement.setString(1, album.getName());
        statement.setInt(2, artistDao.getId(album.getArtist()));
        statement.execute();
    }

    @Override
    public void update(Album album, int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.UPDATE_ALBUM_QUERY);
        statement.setString(1, album.getName());
        statement.setLong(2, id);
        statement.execute();
    }

    @Override
    public void delete(int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.DELETE_ALBUM_QUERY);
        statement.setLong(1, id);
        statement.execute();
    }
    public int getId(Album album) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.FIND_ALBUM_ID_QUERY);
        statement.setString(1, album.getName());
        ResultSet rs = statement.executeQuery();
        rs.next();
        return rs.getInt("id");
    }
}
