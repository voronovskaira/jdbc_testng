package controller;

import constant.MenuMessages;
import db.AlbumDao;
import db.Dao;
import db.SongDao;
import model.Album;
import model.Song;
import util.ConsoleReader;
import util.MenuOptions;
import view.View;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class SongController extends Controller<Song> {

    private Dao<Song> songDao = new SongDao();
    private Dao<Album> albumDao = new AlbumDao();
    private Controller<Album> albumController;

    private View view;
    private ConsoleReader consoleReader;

    public SongController(View view, ConsoleReader consoleReader) {
        this.view = view;
        this.consoleReader = consoleReader;
        this.albumController = new AlbumController(view, consoleReader);
    }

    @Override
    public Song get() {
        Song song = new Song();
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose song to view info", map.keySet());
            song = songDao.get(userChoice);
        } catch (SQLException e) {
            LOG.error("Couldn't get song");
        }
        return song;
    }

    @Override
    public Song create() {
        Song song = new Song();
        try {
            song.setName(consoleReader.scanFromConsole("Please input song name"));
            song.setPrice(consoleReader.readValue("Please input song price"));
            selectAlbum(song);
            songDao.create(song);
            view.print(MenuMessages.RECORD_ADDED + song);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return song;
    }

    @Override
    public Map<Integer, String> getAll() {
        Map<Integer, String> map = new HashMap<>();
        try {
            map = songDao.getAll();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return map;
    }

    @Override
    public void update() {
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose song to update", map.keySet());
            Song song = new Song();
            song.setName(consoleReader.scanFromConsole("Please input song name"));
            selectAlbum(song);
            songDao.update(song, userChoice);
            view.print(MenuMessages.RECORD_UPDATED + song);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public void delete() {
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose song to delete", map.keySet());
            songDao.delete(userChoice);
            view.print(MenuMessages.RECORD_DELETED);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }

    }

    private void selectAlbum(Song song) throws SQLException {
        Map<Integer, String> map = albumDao.getAll();
        view.print(MenuOptions.showMenuAddNewRecord(map));
        int albumId = consoleReader.readValue("Choose album", map.keySet());
        if (albumId == 0) {
            Album album = albumController.create();
            song.setAlbum(album);
        } else
            song.setAlbum(albumDao.get(albumId));
    }

}
