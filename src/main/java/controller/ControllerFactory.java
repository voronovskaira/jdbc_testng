package controller;

import util.ConsoleReader;
import view.View;

import java.util.HashMap;
import java.util.Map;

public class ControllerFactory {

    private Map<Integer, Controller> controllers = new HashMap<>();

    public ControllerFactory(View view, ConsoleReader consoleReader) {
        controllers.put(1, new ArtistController(view, consoleReader));
        controllers.put(2, new MusicLabelController(view, consoleReader));
        controllers.put(3, new AlbumController(view, consoleReader));
        controllers.put(4, new SongController(view, consoleReader));
    }

    public Controller getMenu(int menuIndex) {
        return controllers.get(menuIndex);
    }

}
