package constant;

public class Query {

    public static String GET_MUSIC_LABEL_QUERY = "SELECT * FROM label WHERE id = (?)";
    public static String GET_ALL_MUSIC_LABELS_QUERY = "SELECT id, name FROM label";
    public static String ADD_MUSIC_LABEL_QUERY = "INSERT INTO label (name) VALUES (?);";
    public static String UPDATE_MUSIC_LABEL_QUERY = "UPDATE label SET name=(?) WHERE id=(?)";
    public static String DELETE_MUSIC_LABEL_QUERY = "DELETE FROM label WHERE id=(?)";
    public static String FIND_LABEL_ID_QUERY = "SELECT id FROM label WHERE name = (?)";
    public static String FIND_ARTIST_ID_QUERY = "SELECT id FROM artist WHERE name = (?)";
    public static String FIND_ALBUM_ID_QUERY = "SELECT id FROM album WHERE name = (?)";
    public static String GET_ARTIST_QUERY = "SELECT artist.name, label.name AS label_name FROM artist JOIN label ON artist.label = label.id WHERE artist.id = (?)";
    public static String GET_ALL_ARTIST_QUERY = "SELECT id, name FROM artist";
    public static String GET_ALL_ALBUM_QUERY = "SELECT id, name FROM album";
    public static String GET_ALL_SONG_QUERY = "SELECT id, name, price FROM song";
    public static String ADD_ARTIST_QUERY = "INSERT INTO artist (name, label) VALUES (?, ?);";
    public static String ADD_ALBUM_QUERY = "INSERT INTO album (name, artist) VALUES (?, ?);";
    public static String ADD_SONG_QUERY = "INSERT INTO song (name, album, price) VALUES (?, ? ,?);";
    public static String UPDATE_ARTIST_QUERY = "UPDATE artist SET name=(?) WHERE id=(?)";
    public static String UPDATE_ALBUM_QUERY = "UPDATE album SET name=(?) WHERE id=(?)";
    public static String UPDATE_SONG_QUERY = "UPDATE song SET name=(?) WHERE id=(?)";
    public static String DELETE_ARTIST_QUERY = "DELETE FROM artist WHERE id=(?)";
    public static String DELETE_ALBUM_QUERY = "DELETE FROM album WHERE id=(?)";
    public static String DELETE_SONG_QUERY = "DELETE FROM song WHERE id=(?)";
    public static String GET_ALBUM_QUERY = "SELECT name, artist FROM album WHERE id = (?)";
    public static String GET_SONG_QUERY = "SELECT name, price, album FROM song WHERE id = (?)";


}
