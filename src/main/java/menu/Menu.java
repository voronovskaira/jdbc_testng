package menu;

import controller.Controller;
import util.ConsoleReader;
import util.MenuOptions;
import view.View;

public class Menu {
    View view;
    ConsoleReader consoleReader;
    Controller controller;

    public Menu(View view, Controller controller) {
        this.view = view;
        this.consoleReader = new ConsoleReader(view);
        this.controller = controller;
    }

    void chooseOperation() {
        view.print(MenuOptions.showMenu(OperationList.getOperations()));
        int userChoice = consoleReader.readValue("Choose operation to execute", OperationList.getOperations().keySet());
        if (userChoice == 1)
            create();
        else if (userChoice == 2)
            read();
        else if (userChoice == 3)
            update();
        else if (userChoice == 4)
            delete();
    }

    private void create() {
        controller.create();
    }

    private void read() {
        view.print(controller.get().toString());
    }

    private void update() {
        controller.update();
    }

    private void delete() {
        controller.delete();
    }
}
