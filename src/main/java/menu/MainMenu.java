package menu;

import constant.MenuMessages;
import controller.Controller;
import controller.ControllerFactory;
import util.ConsoleReader;
import util.MenuOptions;
import view.View;

public class MainMenu {

    private View view;
    private ControllerFactory controllerFactory;
    private ConsoleReader consoleReader;

    public MainMenu(View view) {
        this.view = view;
        this.consoleReader = new ConsoleReader(view);
        this.controllerFactory = new ControllerFactory(view, consoleReader);

    }

    public void showTableList() {
        view.print(MenuOptions.showMenu(TablesList.getTablesList()));
    }

    public void run() {
        int userChoice;
        do {
            showTableList();
            userChoice = consoleReader.readValue(MenuMessages.FIRST_MESSAGE, TablesList.getTablesList().keySet());
            Controller controller = controllerFactory.getMenu(userChoice);
            Menu menu = new Menu(view, controller);
            menu.chooseOperation();
        } while (userChoice != 0);
    }
}
