package model;

public class User {
    String name;
    Song currentSong;
    String email;
    String password;
    Song song;

    public User(String name, Song currentSong, String email, String password, Song song) {
        this.name = name;
        this.currentSong = currentSong;
        this.email = email;
        this.password = password;
        this.song = song;
    }
}
