package model;

public class Song {
    String name;
    int price;
    Album album;

    public Song(String name, int price, Album album) {
        this.name = name;
        this.price = price;
        this.album = album;
    }

    public Song() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", album=" + album +
                '}';
    }
}
