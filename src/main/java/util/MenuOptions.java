package util;

import java.util.Map;

public class MenuOptions {

    public static String showMenu(Map<Integer, String> map) {
        StringBuilder sb = new StringBuilder();
        map.forEach((k, v) -> sb.append(k).append(" - ").append(v).append("\n"));
        return sb.toString();
    }

    public static String showMenuAddNewRecord(Map<Integer, String> map) {
        StringBuilder sb = new StringBuilder();
        map.put(0, "Create new record");
        map.forEach((k, v) -> sb.append(k).append(" - ").append(v).append("\n"));
        return sb.toString();
    }
}
