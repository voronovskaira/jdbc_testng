package util;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import view.ConsoleView;

import java.util.HashSet;
import java.util.Set;

public class ConsoleReaderTest {
    private static Logger LOG = LogManager.getLogger(PropertyReader.class);
    private ConsoleReader cr = new ConsoleReader(new ConsoleView());

    @Test
    public void checkRangeTest(){
        LOG.debug("Test for check range");
        Set<Integer> key = new HashSet<>();
        key.add(1);
        key.add(2);
        key.add(3);
        Assert.assertTrue(cr.checkRange(2, key));
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void checkRangeTest2(){
        LOG.debug("Test for check range and throw exception");
        Set<Integer> key = new HashSet<>();
        key.add(1);
        key.add(2);
        key.add(3);
        cr.checkRange(5, key);
    }


}
